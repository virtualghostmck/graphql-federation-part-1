const resolvers = {
  Query: {
    latestReviews: (_, __, { dataSources }) => {
      return dataSources.reviewsAPI.getLatestReviews();
    },
  },
  Mutation: {
    submitReview: (_, { locationReview }, { dataSources }) => {
      const newReview =
        dataSources.reviewsAPI.submitReviewForLocation(locationReview);
      return {
        code: 200,
        success: true,
        message: "success",
        locationReview: newReview,
      };
    },
  },
  Location: {
    overallRating: (location, _, context) => {
      const { dataSources } = context;
      return dataSources.reviewsAPI.getOverallRatingForLocation(location.id);
    },
    reviewsForLocation: (location, _, context) => {
      const { dataSources } = context;
      return dataSources.reviewsAPI.getReviewsForLocation(location.id);
    },
  },
  Review: {
    location: (review) => {
      return { id: review.locationId };
    },
  },
};

module.exports = resolvers;
