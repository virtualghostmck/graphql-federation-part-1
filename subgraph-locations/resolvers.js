const resolvers = {
  Query: {
    locations: (_, __, { dataSources }) => {
      return dataSources.locationsAPI.getAllLocations();
    },
    location: (_, { id }, { dataSources }) => {
      return dataSources.locationsAPI.getLocation(id);
    },
  },
  Location: {
    __resolveReference: (representationObject, context) => {
      const { id } = representationObject;
      const { dataSources } = context;
      return dataSources.locationsAPI.getLocation(id);
    },
  },
};

module.exports = resolvers;
